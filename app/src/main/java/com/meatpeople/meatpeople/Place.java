package com.meatpeople.meatpeople;

import android.location.Location;

/**
 * Created by Matt on 8/10/2014.
 */
public class Place {
    public String name;

    public String icon;

    public String place_id;

    public String formatted_address;

    public String formatted_phone_number;

    public Location location;
}
