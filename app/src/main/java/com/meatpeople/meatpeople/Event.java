package com.meatpeople.meatpeople;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Matt on 8/10/2014.
 */
@ParseClassName("Event")
public class Event extends ParseObject {
    public Event(){

    }

    public String getName(){
        return getString("name");
    };

    public Date getDate(){
        return getDate("eventDate");
    }

    public String getType(){
        return getString("type");
    }

    public String getOwnerId(){
        return getString("ownerId");
    }

    public String getInviteeIds(){
        return getString("inviteeIds");
    }

    public JSONObject getDestination(){
        return getJSONObject("destination");
    }

    public void setName(String value){
        put("name", value);
    }

    public void setDate(Date value){
        put("eventDate", value);
    }

    public void setType(String value){
        put("type", value);
    }

    public void setOwnerId(String value){
        put("ownerId", value);
    }

    public void setInviteeIds(String value){
        put("inviteeIds", value);
    }

    public void setDestination(JSONObject value){
        put("destination", value);
    }

    public int getNumberInvited(){
        String[] invited = this.getInviteeIds().split(";");
        return invited.length;
    }
}
