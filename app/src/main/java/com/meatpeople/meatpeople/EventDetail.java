package com.meatpeople.meatpeople;

import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.meatpeople.meatpeople.R;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class EventDetail extends ActionBarActivity implements
        GooglePlayServicesClient.OnConnectionFailedListener,
        GooglePlayServicesClient.ConnectionCallbacks{

    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private final static int REQUEST_CODE_RECOVER_PLAY_SERVICES = 1001;
    private static LocationClient locationClient = null;

    public static ArrayList<ParseUser> acceptedUsers = new ArrayList<ParseUser>();
    public static ArrayList<ParseUser> declinedUsers = new ArrayList<ParseUser>();
    public static ArrayList<ParseUser> notRespondingUsers = new ArrayList<ParseUser>();

    public static String eventTitle = new String();
    public static String eventType = new String();
    public static String eventTime = new String();
    public static String eventId = new String();
    public static int ownerType;//0 = host, 1 = guest
    public static boolean responded;

    public void GetAttendingUsers(){
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("eventId", eventId);
        ParseCloud.callFunctionInBackground("getAttendingUsers", params, new FunctionCallback<List<ParseUser>>() {
            public void done(List<ParseUser> result, ParseException e) {
                if (e == null) {
                    for(ParseUser user : result){
                        acceptedUsers.add(user);
                        if (user == ParseUser.getCurrentUser()){
                            responded = true;
                        }
                    }
                }
                GetDecliningUsers();
            }
        });
    }

    public void GetDecliningUsers(){
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("eventId", eventId);
        ParseCloud.callFunctionInBackground("getDecliningUsers", params, new FunctionCallback<List<ParseUser>>() {
            public void done(List<ParseUser> result, ParseException e) {
                if (e == null) {
                    for(ParseUser user : result){
                        declinedUsers.add(user);
                        if (user == ParseUser.getCurrentUser()){
                            responded = true;
                        }
                    }
                }
                GetNotRespondedUsers();
            }
        });
    }

    public void GetNotRespondedUsers(){
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("eventId", eventId);
        ParseCloud.callFunctionInBackground("getNotRespondingUsers", params, new FunctionCallback<List<ParseUser>>() {
            public void done(List<ParseUser> result, ParseException e) {
                if (e == null) {
                    for(ParseUser user : result){
                        notRespondingUsers.add(user);
                    }
                }
                //maybe put code here
            }
        });
    }

    void showErrorDialog(int code) {
        GooglePlayServicesUtil.getErrorDialog(code, this,
                REQUEST_CODE_RECOVER_PLAY_SERVICES).show();
    }

    @Override
    public void onConnected(Bundle bundle) {
        //Display the connection status
        Toast.makeText(this, "Connected", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDisconnected() {
        // Display the connection status
        Toast.makeText(this, "Disconnected. Please re-connect.",
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(
                        this,
                        CONNECTION_FAILURE_RESOLUTION_REQUEST);
                /*
                 * Thrown if Google Play services canceled the original
                 * PendingIntent
                 */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
            /*
             * If no resolution is available, display a dialog to the
             * user with the error.
             */
            showErrorDialog(connectionResult.getErrorCode());
        }
    }

    /*
         * Handle results returned to the FragmentActivity
         * by Google Play services
         */
    @Override
    protected void onActivityResult(
            int requestCode, int resultCode, Intent data) {
        // Decide what to do based on the original request code
        switch (requestCode) {
            case CONNECTION_FAILURE_RESOLUTION_REQUEST :
            /*
             * If the result code is Activity.RESULT_OK, try
             * to connect again
             */
                switch (resultCode) {
                    case Activity.RESULT_OK :
                        locationClient.connect();
                        break;
                }

        }
    }

    private boolean servicesConnected() {
        // Check that Google Play services is available
        int resultCode =
                GooglePlayServicesUtil.
                        isGooglePlayServicesAvailable(this);
        // If Google Play services is available
        if (ConnectionResult.SUCCESS == resultCode) {
            // In debug mode, log the status
            Log.d("Location Updates",
                    "Google Play services is available.");
            // Continue
            return true;
            // Google Play services was not available for some reason
        } else {
            // Get the error code
            ConnectionResult connectionResult =
                    new ConnectionResult(resultCode,
                            PendingIntent.getActivity(
                                    this,
                                    1,
                                    new Intent(
                                            Intent.ACTION_APP_ERROR),
                                    PendingIntent.FLAG_CANCEL_CURRENT
                            ));
            int errorCode = connectionResult.getErrorCode();
            // Get the error dialog from Google Play services
            Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
                    errorCode,
                    this,
                    CONNECTION_FAILURE_RESOLUTION_REQUEST);

            // If Google Play services can provide an error dialog
            if (errorDialog != null) {
                // Create a new DialogFragment for the error dialog
                ErrorDialogFragment errorFragment =
                        new ErrorDialogFragment();
                // Set the dialog in the DialogFragment
                errorFragment.setDialog(errorDialog);
                // Show the error dialog in the DialogFragment
                errorFragment.show(getFragmentManager(),
                        "Location Updates");
            }
        }
        return false;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        GetAttendingUsers();

        boolean hasGooglePlayServices = servicesConnected();
        if(hasGooglePlayServices) {
            locationClient = new LocationClient(this, this, this);
        }

        setContentView(R.layout.activity_event_detail);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new EventDetailFragment())
                    .commit();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        locationClient.connect();
    }

    @Override
    protected void onStop() {
        locationClient.disconnect();
        super.onStop();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.event_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public static class EventDetailFragment extends Fragment {

        public EventDetailFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {

            Intent intent = getActivity().getIntent();

            if(intent!=null){
                Bundle extras = intent.getExtras();
                if(extras!=null){
                    eventTitle = extras.getString("EVENT_TITLE");
                    eventType = extras.getString("EVENT_TYPE");
                    eventTime = extras.getString("EVENT_TIME");
                    eventId = extras.getString("EVENT_ID");
                    ownerType = extras.getInt("OWNER_TYPE");
                }
            }

            ArrayList<String> guestHeaderStrings = new ArrayList<String>();
            HashMap<String, List<ParseUser>> guestChildren = new HashMap<String, List<ParseUser>>();
            guestHeaderStrings.add("Guests Attending");
            guestHeaderStrings.add("Guests Not Responded");
            guestHeaderStrings.add("Guests Not Attending");

            guestChildren.put(guestHeaderStrings.get(0), acceptedUsers);
            guestChildren.put(guestHeaderStrings.get(1), notRespondingUsers);
            guestChildren.put(guestHeaderStrings.get(2), declinedUsers);
            GuestExpandableListAdapter expandableListAdapter = new GuestExpandableListAdapter(getActivity(),guestHeaderStrings,guestChildren);

            View rootView = inflater.inflate(R.layout.fragment_event_detail, container, false);

            getActivity().setTitle(eventTitle);

            TextView eventTypeTextView = (TextView) rootView.findViewById(R.id.event_detail_type_textview);
            TextView eventTimeTextView = (TextView) rootView.findViewById(R.id.event_detail_time_textview);
            eventTypeTextView.setText(eventType);
            eventTimeTextView.setText(eventTime);

            ExpandableListView expandableListView = (ExpandableListView) rootView.findViewById(R.id.expandable_listview_guests);
            expandableListView.setAdapter(expandableListAdapter);
            //if user is a host
            if(ownerType == 0) {
                Button submitButton = (Button) rootView.findViewById(R.id.button_find_places);
                submitButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onSubmitClicked();
                    }
                });
                Button yesButton = (Button) rootView.findViewById(R.id.button_attending_yes);
                yesButton.setEnabled(false);
                yesButton.setVisibility(View.INVISIBLE);
                Button noButton = (Button) rootView.findViewById(R.id.button_attending_no);
                yesButton.setEnabled(false);
                noButton.setVisibility(View.INVISIBLE);
            }
            else if(ownerType == 1){
                Button submitButton = (Button) rootView.findViewById(R.id.button_find_places);
                submitButton.setVisibility(View.INVISIBLE);
                final Button yesButton = (Button) rootView.findViewById(R.id.button_attending_yes);
                final Button noButton = (Button) rootView.findViewById(R.id.button_attending_no);
                if (responded){
                    yesButton.setEnabled(false);
                    noButton.setEnabled(false);
                    yesButton.setVisibility(View.INVISIBLE);
                    noButton.setVisibility(View.INVISIBLE);
                }
                yesButton.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        Location currentLocation = locationClient.getLastLocation();
                        BackendAccess.AddResponse(eventId, currentLocation, true);
                        yesButton.setEnabled(false);
                        noButton.setEnabled(false);
                        yesButton.setVisibility(View.INVISIBLE);
                        noButton.setVisibility(View.INVISIBLE);

                    }
                });
                noButton.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        //Location currentLocation = locationClient.getLastLocation();
                        BackendAccess.AddResponse(eventId, null, false);
                        yesButton.setEnabled(false);
                        noButton.setEnabled(false);
                        yesButton.setVisibility(View.INVISIBLE);
                        noButton.setVisibility(View.INVISIBLE);
                    }
                });

            }
            return rootView;
        }

        public void onSubmitClicked(){
            //BackendAccess.GetMidpointAndPlaces(eventId, eventType, 1000);
            Intent intent = new Intent(getActivity(),PlacesActivity.class);
            intent.putExtra("EVENT_ID", eventId);
            intent.putExtra("EVENT_TYPE", eventType);
            startActivity(intent);
        }


    }
}
