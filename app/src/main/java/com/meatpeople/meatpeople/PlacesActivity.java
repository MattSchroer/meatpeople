package com.meatpeople.meatpeople;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.meatpeople.meatpeople.R;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PlacesActivity extends Activity {

    private static String eventId;
    private static String eventType;
    private static final int radius = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Intent intent = getIntent();
        eventId = intent.getStringExtra("EVENT_ID");
        eventType = intent.getStringExtra("EVENT_TYPE");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_places);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PlacesFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.places, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if(id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlacesFragment extends Fragment {

        public PlacesFragment() {
        }

        private ArrayAdapter<String> placesAdapter;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {

            placesAdapter = new ArrayAdapter<String>(
                    getActivity(),
                    R.layout.list_item_places,
                    R.id.list_item_places_textview,
                    new ArrayList<String>());

            View rootView = inflater.inflate(R.layout.fragment_places, container, false);
            ListView listView = (ListView) rootView.findViewById(R.id.places_listvew);
            listView.setAdapter(placesAdapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    //TODO: make selection based on click
                }
            });

            return rootView;
        }

        private void getPlaces(){

            HashMap<String, Object> params = new HashMap<String, Object>();
            params.put("eventId", eventId);
            ParseCloud.callFunctionInBackground("calculateMidpoint", params, new FunctionCallback<ParseGeoPoint>() {
                public void done(ParseGeoPoint result, ParseException e) {
                    if (e == null) {
                        if (result != null) {
                            //String[] split = result.split("|");
                            PlacesService placesService = new PlacesService();
                            String[] searchArray = {eventType, String.valueOf(result.getLatitude()), String.valueOf(result.getLongitude()), Integer.toString(radius)};
                            placesService.execute(searchArray);
                        }
                    }
                }
            });

        }

        @Override
        public void onStart(){
            super.onStart();
            getPlaces();
        }

        public class PlacesService extends AsyncTask<String[], Void, ArrayList<Place>> {
            private static final String LOG_TAG = "MeatPeople";

            private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";

            private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
            private static final String TYPE_DETAILS = "/details";
            private static final String TYPE_SEARCH = "/search";

            private static final String OUT_JSON = "/json";

            // KEY!
            private static final String API_KEY = "AIzaSyApyrSaN_cmHKfhRZhZj8d6MYFK0wA5JSk";

            protected ArrayList<Place> doInBackground(String[]... params){
                //system calls this to perform work in a worker thread and delivers
                // it the parameters given to AsyncTask.execute
                //[0] = type, [1] = lat, [2] = lng, [3] = radius
                String[] searchInput = params[0];
                int radius = Integer.parseInt(searchInput[3]);
                return search(searchInput[0],searchInput[1],searchInput[2],radius);
            }

            protected void onPostExecute(ArrayList<Place> places){
                if(places != null){
                    placesAdapter.clear();
                    for(Place place : places){
                        String formattedString = place.name/* + " - " + String.valueOf(place.location.getLatitude()) + "and" + String.valueOf(place.location.getLongitude())*/;
                        placesAdapter.add(formattedString);
                        if(place.location!=null) {
                            Log.d(LOG_TAG, String.valueOf(place.location.getLongitude()));
                        }
                    }
                }
            }

            public ArrayList<Place> search(String type, String lat, String lng, int radius){
                ArrayList<Place> resultList = null;

                HttpURLConnection conn = null;
                StringBuilder jsonResults = new StringBuilder();
                try {
                    StringBuilder sb = new StringBuilder(PLACES_API_BASE);
                    sb.append(TYPE_SEARCH);
                    sb.append(OUT_JSON);
                    sb.append("?sensor=false");
                    sb.append("&key=" + API_KEY);
                    sb.append("&types=" + URLEncoder.encode(type, "utf8"));
                    sb.append("&location=" + lat + "," + lng);
                    sb.append("&opennow");
                    sb.append("&radius=" + String.valueOf(radius));

                    URL url = new URL(sb.toString());
                    conn = (HttpURLConnection) url.openConnection();
                    InputStreamReader in = new InputStreamReader(conn.getInputStream());

                    int read;
                    char[] buff = new char[1024];
                    while ((read = in.read(buff)) != -1) {
                        jsonResults.append(buff, 0, read);
                    }
                } catch (MalformedURLException e) {
                    Log.e(LOG_TAG, "Error processing Places API URL", e);
                    return resultList;
                } catch (IOException e) {
                    Log.e(LOG_TAG, "Error connecting to Places API", e);
                    return resultList;
                } finally {
                    if (conn != null) {
                        conn.disconnect();
                    }
                }

                try {
                    // Create a JSON object hierarchy from the results
                    JSONObject jsonObj = new JSONObject(jsonResults.toString());
                    JSONArray predsJsonArray = jsonObj.getJSONArray("results");

                    // Extract the Place descriptions from the results
                    resultList = new ArrayList<Place>(predsJsonArray.length());
                    for (int i = 0; i < predsJsonArray.length(); i++) {
                        Place place = new Place();
                        place.place_id = predsJsonArray.getJSONObject(i).getString("place_id");
                        place.name = predsJsonArray.getJSONObject(i).getString("name");
                        resultList.add(place);
                    }
                } catch (JSONException e) {
                    Log.e(LOG_TAG, "Error processing JSON results", e);
                }

                return resultList;
            }

            public Place details(String place_id) {
                HttpURLConnection conn = null;
                StringBuilder jsonResults = new StringBuilder();
                try {
                    StringBuilder sb = new StringBuilder(PLACES_API_BASE);
                    sb.append(TYPE_DETAILS);
                    sb.append(OUT_JSON);
                    sb.append("?sensor=false");
                    sb.append("&key=" + API_KEY);
                    sb.append("&placeid=" + URLEncoder.encode(place_id, "utf8"));

                    URL url = new URL(sb.toString());
                    conn = (HttpURLConnection) url.openConnection();
                    InputStreamReader in = new InputStreamReader(conn.getInputStream());

                    // Load the results into a StringBuilder
                    int read;
                    char[] buff = new char[1024];
                    while ((read = in.read(buff)) != -1) {
                        jsonResults.append(buff, 0, read);
                    }
                } catch (MalformedURLException e) {
                    Log.e(LOG_TAG, "Error processing Places API URL", e);
                    return null;
                } catch (IOException e) {
                    Log.e(LOG_TAG, "Error connecting to Places API", e);
                    return null;
                } finally {
                    if (conn != null) {
                        conn.disconnect();
                    }
                }

                Place place = null;
                try {
                    // Create a JSON object hierarchy from the results
                    JSONObject jsonObj = new JSONObject(jsonResults.toString()).getJSONObject("result");

                    place = new Place();
                    place.icon = jsonObj.getString("icon");
                    place.name = jsonObj.getString("name");
                    place.formatted_address = jsonObj.getString("formatted_address");
                    if (jsonObj.has("formatted_phone_number")) {
                        place.formatted_phone_number = jsonObj.getString("formatted_phone_number");
                    }
                } catch (JSONException e) {
                    Log.e(LOG_TAG, "Error processing JSON results", e);
                }

                return place;
            }
        }
    }
}
