package com.meatpeople.meatpeople;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;

import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.ParseAnalytics;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseInstallation;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ParseAnalytics.trackAppOpened(getIntent());

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new EventListFragment())
                    .commit();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.action_logout){
            ParseFacebookUtils.getSession().closeAndClearTokenInformation();
            // Log the user out
            ParseUser.logOut();
            ParseInstallation installation = ParseInstallation.getCurrentInstallation();
            if (installation != null){
                installation.remove("user");
                installation.saveInBackground();
            }
            // Go to the login view
            startLoginActivity();
        }

        if(id == R.id.action_new_event){
            Intent newEventIntent = new Intent(this, NewEventActivity.class);
            startActivity(newEventIntent);

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ParseUser currentUser = ParseUser.getCurrentUser();
        if (currentUser == null) {
            startLoginActivity();
        }
    }

    private void startLoginActivity() {
        Intent intent = new Intent(this, Login.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public static class EventListFragment extends Fragment {
        public ArrayList<Event> myEvents = new ArrayList<Event>();
        public ArrayList<Event> invitedEvents = new ArrayList<Event>();

        public EventListFragment() {
            FetchMyEvents();
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            ArrayList<String> eventHeaderStrings = new ArrayList<String>();
            HashMap<String, List<Event>> eventChildren = new HashMap<String, List<Event>>();
            eventHeaderStrings.add("My Events");
            eventHeaderStrings.add("Friends' Events");

            eventChildren.put(eventHeaderStrings.get(0),myEvents);
            eventChildren.put(eventHeaderStrings.get(1),invitedEvents);

            MyExpandableListAdapter expandableListAdapter = new MyExpandableListAdapter(getActivity(),eventHeaderStrings,eventChildren);

            View rootView = inflater.inflate(R.layout.fragment_main, container, false);

            ExpandableListView expandableListView = (ExpandableListView) rootView.findViewById(R.id.expandable_listview_events);
            expandableListView.setAdapter(expandableListAdapter);
            return rootView;
        }

        public void FetchMyEvents(){
            if (ParseUser.getCurrentUser() == null){
                return;
            }
            String user = ParseUser.getCurrentUser().getObjectId();
            ParseQuery<Event> query = ParseQuery.getQuery(Event.class);
            query.whereEqualTo("ownerId", user);
            query.whereGreaterThan("eventDate", new Date());
            query.setLimit(10);
            query.findInBackground(new FindCallback<Event>() {
                public void done(List<Event> eventList, ParseException e) {
                    if (e == null) {
                        for(Event event : eventList){
                            myEvents.add(event);
                        }


                    } else {

                    }
                    FetchInvitedEvents();
                }
            });
        }

        public void FetchInvitedEvents(){
            if (ParseUser.getCurrentUser() == null){
                return;
            }
            String user = ParseUser.getCurrentUser().getObjectId();
            if (user == null){
                return;
            }
            ParseQuery<Event> query = ParseQuery.getQuery(Event.class);
            query.whereContains("inviteeIds", user);
            query.whereGreaterThan("eventDate", new Date());
            query.setLimit(10);
            query.findInBackground(new FindCallback<Event>() {
                public void done(List<Event> eventList, ParseException e) {
                    if (e == null) {
                        for(Event event : eventList){
                            invitedEvents.add(event);
                        }
                    }
                    else {

                    }
                }
            });
        }
    }
}
