package com.meatpeople.meatpeople;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.parse.ParseUser;

import org.json.JSONException;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Matt on 8/11/2014.
 */
public class CustomArrayAdapter extends ArrayAdapter<ParseUser>{
    private int resource;
    private List<ParseUser> users;
    private Context context;

    public CustomArrayAdapter(Context context, int resource, List<ParseUser> objects) {
        super(context, resource, objects);
        this.users = objects;
        this.resource = resource;
        this.context = context;
    }

    public View getView (int position, View convertView, ViewGroup parent){
        if(convertView == null){
            LayoutInflater mLayoutInflater = LayoutInflater.from(context);
            convertView = mLayoutInflater.inflate(android.R.layout.simple_list_item_checked, null);
        }

        ParseUser user = users.get(position);

        TextView descriptionView = (TextView) convertView.findViewById(android.R.id.text1);

        try {
            descriptionView.setText(user.getJSONObject("profile").getString("name"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return convertView;

    }
}
