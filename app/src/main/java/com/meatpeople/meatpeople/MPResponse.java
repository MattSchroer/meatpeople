package com.meatpeople.meatpeople;

import com.parse.ParseClassName;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;

@ParseClassName("Response")
public class MPResponse extends ParseObject {
    public MPResponse(){

    }

    public Boolean getAttending(){
        return getBoolean("attending");
    }

    public String getUserId(){
        return getString("userId");
    }

    public String getEventId(){
        return getString("eventId");
    }

    public ParseGeoPoint getLocation(){
        return getParseGeoPoint("location");
    }

    public void setAttending(Boolean value){
        put("attending", value);
    }

    public void setUserId(String value){
        put("userId", value);
    }

    public void setEventId(String value){
        put("eventId", value);
    }

    public void setLocation(ParseGeoPoint value){
        put("location", value);
    }
}