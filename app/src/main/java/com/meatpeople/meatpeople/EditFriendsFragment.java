package com.meatpeople.meatpeople;

/**
 * Created by Benjamin on 8/11/2014.
 */

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.facebook.Request;
import com.facebook.model.GraphUser;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.List;

public class EditFriendsFragment extends ListFragment {
    public static final String TAG = EditFriendsFragment.class.getSimpleName();
    ListView listView;
    public ArrayList<String> meatFriends = new ArrayList<String>();
    protected List<ParseUser> ParseUsers = new ArrayList<ParseUser>();
    protected ParseUser mCurrentUser;
    protected ParseRelation<ParseUser> mFriendsRelation;
    private CustomArrayAdapter adapter;
    protected List<ParseUser> mUsers;
    protected ParseUser MPUser = new ParseUser();
    public EditFriendsFragment(){
    }

    @Override
    public void onResume() {
        super.onResume();
        mCurrentUser = ParseUser.getCurrentUser();
        mFriendsRelation = mCurrentUser
                .getRelation(ParseConstants.KEY_FRIENDS_RELATION);

        getActivity().setProgressBarIndeterminateVisibility(true);
        // Add this line in order for this fragment to handle menu events.

        Request.executeMyFriendsRequestAsync(ParseFacebookUtils.getSession(), new Request.GraphUserListCallback() {

            @Override
            public void onCompleted(final List<GraphUser> users, com.facebook.Response response) {
                if (users != null) {
                    List<String> friendsList = new ArrayList<String>();
                    for (GraphUser user : users) {
                        friendsList.add(user.getId());
                    }

                    // Construct a ParseUser query that will find friends whose
                    // facebook IDs are contained in the current user's friend list.
                    ParseQuery friendQuery = ParseQuery.getUserQuery();
                    friendQuery.whereContainedIn("fbId", friendsList);

                    // findObjects will return a list of ParseUsers that are friends with
                    // the current user
                    friendQuery.findInBackground(new FindCallback<ParseUser>() {

                        @Override
                        public void done(List<ParseUser> list, ParseException e) {
                            getActivity().setProgressBarIndeterminateVisibility(false);
                            if (e == null) {
                                for (ParseUser user : list) {
                                    ParseUsers.add(user);
                                }

                                //Toast.makeText(getActivity(), ParseUsers.toString(), Toast.LENGTH_SHORT).show();
                                CustomArrayAdapter adapter = new CustomArrayAdapter(
                                       getActivity(),
                                        android.R.layout.simple_list_item_checked,
                                        ParseUsers);
                                setListAdapter(adapter);
                                addFriendCheckmarks();

                            } else {
                                Log.e(TAG, e.getMessage());
                                AlertDialog.Builder builder = new AlertDialog.Builder(
                                        getActivity());
                                builder.setTitle(R.string.error_title).setPositiveButton(
                                        android.R.string.ok, null);
                                AlertDialog dialog = builder.create();
                                dialog.show();
                            }
                        }

                    });
                }
            }

        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            //Todo: add settings
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                Bundle savedInstanceState) {

        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        final ListView listView = (ListView) rootView.findViewById(android.R.id.list);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        return rootView;
        }

        @Override
        public void onListItemClick(ListView l, View v, int position, long id) {
            super.onListItemClick(l, v, position, id);

            if (getListView().isItemChecked(position)) {
                // add friend
               mFriendsRelation.add(ParseUsers.get(position));
            } else {
                // remove friend
                mFriendsRelation.remove(ParseUsers.get(position));
            }
            mCurrentUser.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e != null) {
                        Log.e(TAG, e.getMessage());
                    }
                }
            });
        }

    private void addFriendCheckmarks() {
        mFriendsRelation.getQuery().findInBackground(
                new FindCallback<ParseUser>() {

                    @Override
                    public void done(List<ParseUser> friends, ParseException e) {
                        if (e == null) {
                            for (int i = 0; i < ParseUsers.size(); i++) {
                                ParseUser user = ParseUsers.get(i);
                                for (ParseUser friend : friends) {
                                    if (friend.getObjectId().equals(
                                            user.getObjectId())) {
                                        getListView().setItemChecked(i, true);
                                    }
                                }
                            }
                        } else {
                            Log.e(TAG, e.getMessage());
                        }
                    }
                });
    }
    }