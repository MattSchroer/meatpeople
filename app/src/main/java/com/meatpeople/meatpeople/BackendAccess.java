package com.meatpeople.meatpeople;

import android.location.Location;

import com.facebook.Request;
import com.facebook.model.GraphUser;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Matt on 8/7/2014.
 */
public class BackendAccess {

    public static void FindMidpoint(String eventId){
        //begin testing cloud call
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("eventId", "96Wn74yVrh");
        ParseCloud.callFunctionInBackground("calculateMidpoint", params, new FunctionCallback<String>() {
            public void done(String result, ParseException e) {
                if (e == null) {
                    // result is "Hello world!"
                }
            }
        });
    }

    // Use this to make new event
    public static void AddEvent(final Event incoming, final Location location) {
        String id = ParseUser.getCurrentUser().getObjectId();

        incoming.setOwnerId(id);
        incoming.saveInBackground((new SaveCallback() {
            public void done(ParseException e) {
                if (e == null) {
                    String eventId = incoming.getObjectId();
                    AddResponse(eventId, location, true);
                } else {
                    // TODO: figure out failure logic.
                }
            }
        }));
    }

    public static void AddResponse(final String eventId, Location location, boolean attending) {
        String id = ParseUser.getCurrentUser().getObjectId();
        final MPResponse inviteResponse = new MPResponse();
        inviteResponse.setEventId(eventId);
        inviteResponse.setAttending(attending);
        inviteResponse.setUserId(id);
        if (attending) {
            ParseGeoPoint geoLocation = locationToGeoPoint(location);
            inviteResponse.setLocation(geoLocation);
        }

        inviteResponse.saveInBackground((new SaveCallback() {
            public void done(ParseException e) {
                if (e == null) {

                } else {
                    //TODO: add failure logic.
                }
            }
        }));
    }


   /* public static void GetMidpointAndPlaces(final String eventId, final String eventType, final int radius){
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("eventId", eventId);
        ParseCloud.callFunctionInBackground("calculateMidpoint", params, new FunctionCallback<String>() {
            public void done(String result, ParseException e) {
                if (e == null) {
                    if (result != null){
                        String[] split = result.split("|");
                        ArrayList<Place> places = PlacesService.search(eventType, split[0], split[1],radius);
                    }
                }
            }
        });
    }*/



    public static void GetFacebookFriends(){
        Request.executeMyFriendsRequestAsync(ParseFacebookUtils.getSession(), new Request.GraphUserListCallback() {
            @Override
            public void onCompleted(List<GraphUser> users, com.facebook.Response response) {
                if (users != null) {
                    List<String> friendsList = new ArrayList<String>();
                    for (GraphUser user : users) {
                        friendsList.add(user.getId());
                    }

                    // Construct a ParseUser query that will find friends whose
                    // facebook IDs are contained in the current user's friend list.

                    try {
                        GetUsers(friendsList);
                    } catch (ParseException e) {

                    }
                }
            }});
    }

    public static void GetUsers(List<String> friendUsers) throws ParseException {
        // findObjects will return a list of ParseUsers that are friends with
        // the current user
        ParseQuery friendQuery = ParseUser.getQuery();
        friendQuery.whereContainedIn("fbId", friendUsers);

        friendQuery.findInBackground(new FindCallback<ParseUser>() {
            @Override
            public void done(List<ParseUser> list, ParseException e) {
                if (e == null){

                }
                else{
                    // exception here
                }
            }
        });
        //TODO: Call method to set dialog source here.
    }

    public static ParseGeoPoint locationToGeoPoint (Location location) {
        ParseGeoPoint geoPt = new ParseGeoPoint(location.getLatitude(), location.getLongitude());
        return geoPt;
    }
}
