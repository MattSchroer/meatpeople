package com.meatpeople.meatpeople;

import android.app.Application;

import com.facebook.model.GraphUser;
import com.parse.Parse;
import com.parse.ParseFacebookUtils;
import com.parse.ParseObject;
import com.parse.PushService;

import java.util.List;

/**
 * Created by Matt on 8/6/2014.
 */
public class MeatPeopleApplication extends Application {
    public static String TAG = "MeatPeople";

    @Override
    public void onCreate() {
        super.onCreate();
        ParseObject.registerSubclass(Event.class);
        ParseObject.registerSubclass(MPResponse.class);
        Parse.enableLocalDatastore(this);
        Parse.initialize(this, "dqUWztAqdE4HmRBkfeWeguyvoqIRIp7Lt8eUJTHc", "6ZxfKEpXUVoOg2AZyVRWIegyoz1zuEleP9xvI3sZ");
        ParseFacebookUtils.initialize("668927893188791");
        PushService.setDefaultPushCallback(this, MainActivity.class);
    }

    private List<GraphUser> selectedUsers;

    public List<GraphUser> getSelectedUsers() {
        return selectedUsers;
    }

    public void setSelectedUsers(List<GraphUser> selectedUsers) {
        this.selectedUsers = selectedUsers;
    }
}

