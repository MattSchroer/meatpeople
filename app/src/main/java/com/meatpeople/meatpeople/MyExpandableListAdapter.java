package com.meatpeople.meatpeople;

/**
 * Created by Peter on 8/7/14.
 */
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MyExpandableListAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<Event>> _listDataChild;

    public MyExpandableListAdapter(Context context, List<String> listDataHeader,
                                 HashMap<String, List<Event>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final Event event = (Event) getChild(groupPosition, childPosition);
        final String time = event.getDate().toString().substring(11,16);
        final String title = event.getName();
        final String type = event.getType();
        String numAttendees = event.getNumberInvited() + " people invited";
        String detailFormat = title + "\n" + type + "\n" + numAttendees;

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.child_item_events, null);
        }

        convertView.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){

                Intent intent = new Intent(_context, EventDetail.class);
                //Create bundle to pass info to Event Detail Activity
                Bundle extras = new Bundle();
                extras.putString("EVENT_TITLE",title);
                extras.putString("EVENT_TYPE", type);
                extras.putString("EVENT_TIME", time);
                extras.putString("EVENT_ID", event.getObjectId());
                //groupPosition tells EventDetail if user is host or guest
                extras.putInt("OWNER_TYPE", groupPosition);
                intent.putExtras(extras);
                _context.startActivity(intent);
            }
        });

        TextView eventTime = (TextView) convertView.findViewById(R.id.event_time_view);
        TextView eventDetails = (TextView) convertView.findViewById(R.id.event_details);
        ImageView eventFinalized = (ImageView) convertView.findViewById(R.id.event_finalized);
        eventFinalized.setVisibility(View.VISIBLE);
        eventTime.setText(time);
        eventDetails.setText(detailFormat);

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.group_item_events, null);
        }

        TextView event_list_header = (TextView) convertView
                .findViewById(R.id.event_list_header);
        event_list_header.setTypeface(null, Typeface.BOLD);
        event_list_header.setText(headerTitle);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
