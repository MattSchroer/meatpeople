package com.meatpeople.meatpeople;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.ParseUser;

import org.json.JSONException;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Peter on 8/10/14.
 */
public class GuestExpandableListAdapter extends BaseExpandableListAdapter {


    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<ParseUser>> _listDataChild;

    public GuestExpandableListAdapter(Context context, List<String> listDataHeader,
                                   HashMap<String, List<ParseUser>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final ParseUser guest = (ParseUser) getChild(groupPosition, childPosition);
        String name = null;
        try {
            name = guest.getJSONObject("profile").getString("name");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.child_item_guests, null);
        }

        ImageView guestProfilePic = (ImageView) convertView.findViewById(R.id.guest_profile_picture);
        TextView guestName = (TextView) convertView.findViewById(R.id.guest_name);
        ImageView guestResponse = (ImageView) convertView.findViewById(R.id.guest_response_status);
        guestName.setText(name);

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.group_item_guests, null);
        }

        TextView guest_list_header = (TextView) convertView
                .findViewById(R.id.guest_list_header);
        guest_list_header.setTypeface(null, Typeface.BOLD);
        guest_list_header.setText(headerTitle);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}

