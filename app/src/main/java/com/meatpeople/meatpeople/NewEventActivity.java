package com.meatpeople.meatpeople;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class NewEventActivity extends ActionBarActivity implements
        GooglePlayServicesClient.ConnectionCallbacks,
        GooglePlayServicesClient.OnConnectionFailedListener{

    private static boolean userIsInteracting;
    private static final String TAG = NewEventActivity.class.getSimpleName();

    // Global constants
    /*
     * Define a request code to send to Google Play services
     * This code is returned in Activity.onActivityResult
     */
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private final static int REQUEST_CODE_RECOVER_PLAY_SERVICES = 1001;
    private static LocationClient locationClient = null;
    private Location currentLocation = null;
    protected ParseUser mCurrentUser;
    protected List<ParseUser> mFriends = new ArrayList<ParseUser>();
    protected ParseRelation<ParseUser> mFriendsRelation;
    protected static String locationType;
    private SpinnerAdapter typeAdapter;
    private TextView eventTitle;

    void showErrorDialog(int code) {
        GooglePlayServicesUtil.getErrorDialog(code, this,
                REQUEST_CODE_RECOVER_PLAY_SERVICES).show();
    }

    @Override
    public void onConnected(Bundle bundle) {
        //Display the connection status
    }

    @Override
    public void onDisconnected() {
        // Display the connection status
        Toast.makeText(this, "Disconnected. Please re-connect.",
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(
                        this,
                        CONNECTION_FAILURE_RESOLUTION_REQUEST);
                /*
                 * Thrown if Google Play services canceled the original
                 * PendingIntent
                 */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
            /*
             * If no resolution is available, display a dialog to the
             * user with the error.
             */
            showErrorDialog(connectionResult.getErrorCode());
        }
    }

    /*
             * Handle results returned to the FragmentActivity
             * by Google Play services
             */
    @Override
    protected void onActivityResult(
            int requestCode, int resultCode, Intent data) {
        // Decide what to do based on the original request code
        switch (requestCode) {
            case CONNECTION_FAILURE_RESOLUTION_REQUEST :
            /*
             * If the result code is Activity.RESULT_OK, try
             * to connect again
             */
                switch (resultCode) {
                    case Activity.RESULT_OK :
                    locationClient.connect();
                        break;
                }

        }
    }

    private boolean servicesConnected() {
        // Check that Google Play services is available
        int resultCode =
                GooglePlayServicesUtil.
                        isGooglePlayServicesAvailable(this);
        // If Google Play services is available
        if (ConnectionResult.SUCCESS == resultCode) {
            // In debug mode, log the status
            Log.d("Location Updates",
                    "Google Play services is available.");
            // Continue
            return true;
            // Google Play services was not available for some reason
        } else {
            // Get the error code
            ConnectionResult connectionResult =
                    new ConnectionResult(resultCode,
                            PendingIntent.getActivity(
                                    this,
                                    1,
                                    new Intent(
                                            Intent.ACTION_APP_ERROR),
                                    PendingIntent.FLAG_CANCEL_CURRENT));
            int errorCode = connectionResult.getErrorCode();
            // Get the error dialog from Google Play services
            Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
                    errorCode,
                    this,
                    CONNECTION_FAILURE_RESOLUTION_REQUEST);

            // If Google Play services can provide an error dialog
            if (errorDialog != null) {
                // Create a new DialogFragment for the error dialog
                ErrorDialogFragment errorFragment =
                        new ErrorDialogFragment();
                // Set the dialog in the DialogFragment
                errorFragment.setDialog(errorDialog);
                // Show the error dialog in the DialogFragment
                errorFragment.show(getFragmentManager(),
                        "Location Updates");
            }
        }
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCurrentUser = ParseUser.getCurrentUser();
        mFriendsRelation = mCurrentUser
               .getRelation(ParseConstants.KEY_FRIENDS_RELATION);
        NewEventActivity.this.setProgressBarIndeterminateVisibility(true);
        ParseQuery<ParseUser> query = mFriendsRelation.getQuery();
        query.addAscendingOrder(ParseConstants.KEY_USERNAME);
        query.findInBackground(
                new FindCallback<ParseUser>() {

                    @Override
                    public void done(List<ParseUser> friends, ParseException e) {
                        NewEventActivity.this.setProgressBarIndeterminateVisibility(false);
                        if (e == null) {
                            mFriends = friends;
                        }
                    }});

                            boolean hasGooglePlayServices = servicesConnected();
                            if (hasGooglePlayServices) {
                                locationClient = new LocationClient(this, this, this);
                            }


                            setContentView(R.layout.activity_new_event);
                            if (savedInstanceState == null) {
                                getSupportFragmentManager().beginTransaction()
                                        .add(R.id.container, new PlaceholderFragment())
                                        .commit();
                            }
                        }

                        @Override
                        protected void onStart () {
                            super.onStart();
                            locationClient.connect();
                        }

                        @Override
                        protected void onStop () {
                            locationClient.disconnect();
                            super.onStop();

                        }

                        @Override
                        public boolean onCreateOptionsMenu (Menu menu){
                            // Inflate the menu; this adds items to the action bar if it is present.
                            getMenuInflater().inflate(R.menu.new_event, menu);
                            return true;
                        }

                        @Override
                        public boolean onOptionsItemSelected (MenuItem item){
                            // Handle action bar item clicks here. The action bar will
                            // automatically handle clicks on the Home/Up button, so long
                            // as you specify a parent activity in AndroidManifest.xml.
                            int id = item.getItemId();
                            if (id == R.id.action_settings) {
                                return true;
                            }
                            if (id == R.id.action_get_location) {
                                currentLocation = locationClient.getLastLocation();
                            }
                            return super.onOptionsItemSelected(item);
                        }

                    public void onFriendsClick(View view) {
                        Intent intent = new Intent(this, EditFriendsActivity.class);
                        startActivity(intent);
                    }

                    public void onSubmitClick(View view){
                        TimePicker notifyTime = ((TimePicker) findViewById(R.id.timePicker));
                        notifyTime.clearFocus();
                        Calendar cal=Calendar.getInstance();
                        int currHour=cal.get(Calendar.HOUR_OF_DAY);
                        int currMinute=cal.get(Calendar.MINUTE);
                        notifyTime.setCurrentHour(currHour + 1);
                        notifyTime.setCurrentMinute(currMinute);

                        int hour = notifyTime.getCurrentHour();
                        int minute = notifyTime.getCurrentMinute();
                        Date date = new Date();
                        date.setHours(hour);
                        date.setMinutes(minute);
                        eventTitle =  (TextView) findViewById(R.id.editText);
                        String strEventTitle = eventTitle.getText().toString();

                        Event event = new Event();
                        if (strEventTitle != null && mFriends != null && locationType != null) {
                            event.setName(strEventTitle);
                            event.setOwnerId(ParseUser.getCurrentUser().getObjectId());
                            event.setType(locationType);
                            event.setDate(date);
                            event.setOwnerId(ParseUser.getCurrentUser().getObjectId());

                            StringBuilder builder = new StringBuilder();
                            StringBuilder mFriendsToStr = new StringBuilder();
                            String delimiter = ";";
                            mFriendsToStr.append(mFriends.get(0).getObjectId());
                            for (int i = 1; i < mFriends.size(); i++) {
                                mFriendsToStr.append(delimiter);
                                mFriendsToStr.append(mFriends.get(i).getObjectId());
                            }
                            event.setInviteeIds(mFriendsToStr.toString());
                            BackendAccess.AddEvent(event, locationClient.getLastLocation());
                        } else if(strEventTitle == null || mFriends == null || locationType == null){
                            new AlertDialog.Builder(this)
                                    .setTitle("Error")
                                    .setMessage("Please make sure to enter all event info.")
                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // do nothing
                                        }
                                    })

                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();
                        }

                        }




                    @Override
                    public void onUserInteraction() {
                        super.onUserInteraction();
                        userIsInteracting = true;
                    }

                    /**
                     * A placeholder fragment containing a simple view.
                     */
                    public static class PlaceholderFragment extends Fragment {


                        private ArrayAdapter<CharSequence> typeAdapter;

                        public PlaceholderFragment() {
                        }

                        @Override
                        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                                 Bundle savedInstanceState) {



                            final ArrayAdapter<CharSequence> typeAdapter =
                                    ArrayAdapter.createFromResource(
                                            getActivity(), R.array.location_array, android.R.layout.simple_spinner_item);
                            typeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                            final View rootView = inflater.inflate(R.layout.fragment_new_event, container, false);
                            final TextView eventTitle = (TextView) rootView.findViewById(R.id.editText);

                            eventTitle.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                                @Override
                                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                                    EditText eventTitle = (EditText) rootView.findViewById(R.id.editText);
                                    String strEventTitle = eventTitle.getText().toString();
                                    if (TextUtils.isEmpty(strEventTitle)) {
                                        eventTitle.setError("Event title required");
                                    }
                                    return true;
                                }
                            });


                            final Spinner spinner = (Spinner) rootView.findViewById(R.id.spinner);
                            spinner.setAdapter(typeAdapter);

                            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                                    //check if default position from spinner, or actual user input
                                    if (userIsInteracting) {
                                        locationType = typeAdapter.getItem(position).toString();
                                    }
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                }
                            });


                            return rootView;

                        }



                    }

                }
